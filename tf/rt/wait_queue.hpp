#pragma once

#include <tf/rt/fwd.hpp>

#include <wheels/intrusive/list.hpp>

namespace tf::rt {

// ~ Futex for cooperative single-threaded fibers

class WaitQueue {
 public:
  WaitQueue() = default;

  // Non-copyable
  WaitQueue(const WaitQueue&) = delete;
  WaitQueue& operator=(const WaitQueue&) = delete;

  // Non-movable
  WaitQueue(WaitQueue&&) = delete;
  WaitQueue& operator=(WaitQueue&&) = delete;

  ~WaitQueue();

  void Park();

  // Move one fiber to scheduler run queue
  void WakeOne();

  // Move all fibers to scheduler run queue
  void WakeAll();

 private:
  wheels::IntrusiveList<Fiber> waiters_;
};

}  // namespace tf::rt
